# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-08-13
### Added
- Integration with Laravel 5.4 and 5.5.
- Ability to register and run both user and system experiments.
- Console commands.
- Helper functions.
- README.md and LICENSE.md files.
- User verification.

## [2.0.0] - 2018-08-14
### Added
- Helper functions similar to 'interact' and 'convert' but they do not suppress
exceptions.
- Added the Laravel framework as a dev dependency.

### Changed
- The 'interact' and 'convert' helper functions hide exceptions by default.
- README.md

## [2.0.1] - 2018-08-24
### Changed
- Set a default value for the cache lifetime parameter to fix a "first time install" error

## [2.0.2] - 2018-08-27
### Changed
- Fixed error where the configuration value of "cache lifetime in days" parameter was not being scaled to the minutes value expected by the underlying cache driver, leading to early expiry of results data
- Updated README.md with improved install instructions for Laravel v5.4 systems