<?php

namespace HeapsGoodServices\Variant;

class SimpleExperimentFactory
{
    /**
     * @var VariationFactory
     */
    private $variationFactory;

    /**
     * @var WeightedRandomiser
     */
    private $weightedRandomiser;

    /**
     * @var UserVariationRepository
     */
    private $userVariationRepository;

    /**
     * ExperimentFactory constructor.
     * @param VariationFactory $variationFactory
     * @param WeightedRandomiser $weightedRandomiser
     * @param UserVariationRepository $userVariationRepository
     */
    public function __construct(VariationFactory $variationFactory, WeightedRandomiser $weightedRandomiser, UserVariationRepository $userVariationRepository)
    {
        $this->variationFactory = $variationFactory;
        $this->weightedRandomiser = $weightedRandomiser;
        $this->userVariationRepository = $userVariationRepository;
    }

    /**
     * @param string $name
     * @param bool $isSystemExperiment
     * @return Experiment
     */
    function makeExperiment(string $name, bool $isSystemExperiment = false): Experiment {
        return new Experiment(
            $name,
            $isSystemExperiment,
            $this->variationFactory,
            $this->weightedRandomiser,
            $this->userVariationRepository
        );
    }
}
