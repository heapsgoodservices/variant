<?php

namespace HeapsGoodServices\Variant;

use Psr\SimpleCache\CacheInterface;

class UserVerificationRepository
{
    const SCRIPT_VERIFICATION_KEY = 'verification.script';
    const IMAGE_VERIFICATION_KEY = 'verification.image';
    const MULTIPLE_VERIFICATION_KEY = 'verification.multiple';

    /**
     * @var CacheInterface
     */
    private $verificationStore;

    /**
     * SessionVerificationRepository constructor.
     *
     * @param CacheInterface $verificationStore
     */
    public function __construct(CacheInterface $verificationStore)
    {
        $this->verificationStore = $verificationStore;
    }

    /**
     * Check that both script and image verification have passed.
     *
     * @return bool
     */
    function checkMultipleVerifications(): bool {
        return boolval($this->verificationStore->get(self::MULTIPLE_VERIFICATION_KEY));
    }

    /**
     * Set the combined verification scheme to true.
     */
    function multipleVerificationsConfirmed() {
        $this->verificationStore->set(self::MULTIPLE_VERIFICATION_KEY, true);
    }

    /**
     * Check if the script verification has passed.
     *
     * @return bool
     */
    function checkScriptVerification(): bool {
        return boolval($this->verificationStore->get(self::SCRIPT_VERIFICATION_KEY));
    }

    /**
     * Set the script verification to true.
     */
    function scriptVerificationConfirmed() {
        $this->verificationStore->set(self::SCRIPT_VERIFICATION_KEY, true);
    }

    /**
     * Check if the image verification has passed.
     *
     * @return bool
     */
    function checkImageVerification() {
        return boolval($this->verificationStore->get(self::IMAGE_VERIFICATION_KEY));
    }

    /**
     * Set the image verification to true.
     */
    function imageVerificationConfirmed() {
        $this->verificationStore->set(self::IMAGE_VERIFICATION_KEY, true);
    }
}
