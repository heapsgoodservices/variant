<?php

namespace HeapsGoodServices\Variant;

interface EventRepository
{
    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getInteractions(string $experiment, string $variation): int;

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function addInteraction(string $experiment, string $variation, int $interactions = 1);

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function setInteraction(string $experiment, string $variation, int $interactions);

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getConversions(string $experiment, string $variation): int;

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function addConversion(string $experiment, string $variation, int $conversions = 1);

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function setConversions(string $experiment, string $variation, int $conversions);
}
