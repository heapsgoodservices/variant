<?php

namespace HeapsGoodServices\Variant;

class Experiment
{
    /**
     * @var string $name
     */
    private $name;

    /**
     * @var bool $isSystemExperiment
     */
    private $isSystemExperiment;

    /**
     * @var array|Variation[] $variations
     */
    private $variations = [];

    /**
     * @var VariationFactory $variationFactory
     */
    private $variationFactory;

    /**
     * @var WeightedRandomiser $weightedRandomiser
     */
    private $weightedRandomiser;

    /**
     * @var UserVariationRepository $userVariationRepository
     */
    private $userVariationRepository;

    /**
     * Experiment constructor.
     *
     * @param string $name
     * @param bool $isSystemExperiment
     * @param VariationFactory $variationFactory
     * @param WeightedRandomiser $weightedRandomiser
     * @param UserVariationRepository $userVariationRepository
     */
    public function __construct(
        string $name,
        bool $isSystemExperiment,
        VariationFactory $variationFactory,
        WeightedRandomiser $weightedRandomiser,
        UserVariationRepository $userVariationRepository
    ) {
        $this->name = $name;
        $this->isSystemExperiment = $isSystemExperiment;
        $this->variationFactory = $variationFactory;
        $this->weightedRandomiser = $weightedRandomiser;
        $this->userVariationRepository = $userVariationRepository;
    }

    /**
     * @param string $name
     * @param int $weight The relative likelihood this variation is retrieved.
     * @return Experiment
     */
    function addVariation(string $name, int $weight = 1): self {
        $variation = $this->variationFactory->makeVariation(
            $this->getName(),
            $name,
            $weight,
            $this->isSystemExperiment
        );

        $this->variations[$name] = $variation;

        return $this;
    }

    /**
     * @return array|Variation[]
     */
    function getVariations(): array {
        return $this->variations;
    }

    /**
     * @return string
     */
    function getName(): string {
        return $this->name;
    }

    /**
     * @return Variation
     */
    function getVariation(): Variation {
        if($this->isSystemExperiment) {
            return $this->getWeightedRandomVariation();
        }

        $cachedVariationName = $this->userVariationRepository->getVariationForExperiment($this);
        if($cachedVariationName) {
            // Search for the variation in the list of variations.
            $searchedVariations = array_filter($this->getVariations(), function(Variation $variation) use ($cachedVariationName) {
                return $cachedVariationName === $variation->getName();
            });

            if(!$searchedVariations) {
                throw new \UnexpectedValueException("Name of experiment variation is invalid.");
            }

            return $searchedVariations[$cachedVariationName];
        }

        $variation = $this->getWeightedRandomVariation();
        $this->userVariationRepository->setVariationForExperiment(
            $this,
            $variation
        );

        return $variation;
    }

    /**
     * @return Variation
     */
    private function getWeightedRandomVariation(): Variation {
        return $this->weightedRandomiser->getRandom($this->getVariations(), function(Variation $variation) {
            return $variation->getWeight();
        });
    }
}
