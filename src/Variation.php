<?php

namespace HeapsGoodServices\Variant;

class Variation
{
    /**
     * @var string
     */
    private $experimentName;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $weight;

    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * Variation constructor.
     *
     * @param string $experimentName
     * @param string $name
     * @param int $weight
     * @param EventRepository $eventRepository
     */
    public function __construct(string $experimentName, string $name, int $weight, EventRepository $eventRepository)
    {
        $this->experimentName = $experimentName;
        $this->name = $name;
        $this->weight = $weight;
        $this->eventRepository = $eventRepository;
    }

    function getExperimentName(): string {
        return $this->experimentName;
    }

    /**
     * @return string
     */
    function getName(): string {
        return $this->name;
    }

    /**
     * @return int
     */
    function getWeight(): int {
        return $this->weight;
    }

    /**
     * @return Variation
     */
    function addInteraction(): self {
        $this->eventRepository->addInteraction($this->getExperimentName(), $this->getName());

        return $this;
    }

    /**
     * @return Variation
     */
    function addConversion(): self {
        $this->eventRepository->addConversion($this->getExperimentName(), $this->getName());

        return $this;
    }
}
