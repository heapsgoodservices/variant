<?php

namespace HeapsGoodServices\Variant;

use Closure;

class WeightedRandomiser
{
    /**
     * @param array $items
     * @param Closure $weightRetriever
     * @return mixed
     */
    function getRandom(array $items, Closure $weightRetriever) {
        if(empty($items)) {
            return null;
        }

        $weightedIndexes = [];

        foreach($items as $itemIndex => $item) {
            $weight = $weightRetriever($item);

            for($i = 0; $i < $weight; $i++) {
                $weightedIndexes[] = $itemIndex;
            }
        }

        // Retrieve the a random key (not value) from the weighted indexes.
        $selectedWeightedIndex = array_rand($weightedIndexes, 1);

        // Retrieve the index of the selected item from the weighted indexes.
        $selectedItemIndex = $weightedIndexes[$selectedWeightedIndex];

        // Return the selected item.
        return $items[$selectedItemIndex];
    }
}
