<?php

namespace HeapsGoodServices\Variant;

use InvalidArgumentException;

class Experiments
{
    /**
     * @var array|Experiment[] $experiments
     */
    private $experiments;

    /**
     * ExperimentStore constructor.
     *
     * @param array $experiments
     */
    public function __construct(array $experiments)
    {
        $this->experiments = $experiments;
    }

    /**
     * @param Experiment $experiment
     * @return Experiments
     */
    function addExperiment(Experiment $experiment): self {
        $this->experiments[$experiment->getName()] = $experiment;

        return $this;
    }

    /**
     * @param string $experimentName
     * @return Experiment
     * @throws InvalidArgumentException
     */
    function getExperiment(string $experimentName): Experiment {
        if(!$this->hasExperiment($experimentName)) {
            throw new InvalidArgumentException("Invalid experiment.");
        }

        return $this->experiments[$experimentName];
    }

    /**
     * @param string $experimentName
     * @return bool
     */
    function hasExperiment(string $experimentName): bool {
        return isset($this->experiments[$experimentName]);
    }

    /**
     * @return array|Experiment[]
     */
    function getExperiments(): array {
        return $this->experiments;
    }
}
