<?php

namespace HeapsGoodServices\Variant\Event;

use HeapsGoodServices\Variant\EventRepository;
use Psr\SimpleCache\CacheInterface;

class AggregateRepository implements EventRepository
{
    /**
     * @var CacheInterface
     */
    private $aggregateStore;

    /**
     * @var int
     */
    private $maxLifetime;

    /**
     * @var KeyGenerator
     */
    private $keyGenerator;

    /**
     * AggregateEventRepository constructor.
     *
     * @param CacheInterface $aggregateStore
     * @param KeyGenerator $keyGenerator
     * @param int $maxLifetime
     */
    public function __construct(CacheInterface $aggregateStore, KeyGenerator $keyGenerator, int $maxLifetime = null) {
        $this->aggregateStore = $aggregateStore;
        $this->maxLifetime = $maxLifetime;
        $this->keyGenerator = $keyGenerator;
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function getInteractions(string $experiment, string $variation): int {
        return intval($this->aggregateStore->get(
            $this->keyGenerator->generateInteractionKey($experiment, $variation)
        ));
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function setInteraction(
        string $experiment,
        string $variation,
        int $interactions
    ) {
        $this->aggregateStore->set(
            $this->keyGenerator->generateInteractionKey($experiment, $variation),
            $interactions,
            $this->maxLifetime
        );
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function addInteraction(
        string $experiment,
        string $variation,
        int $interactions = 1
    ) {
        $currentInteractions = $this->getInteractions($experiment, $variation);
        $this->setInteraction(
            $experiment,
            $variation,
            $currentInteractions + $interactions
        );
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function getConversions(string $experiment, string $variation): int {
        return intval($this->aggregateStore->get(
            $this->keyGenerator->generateConversionKey($experiment, $variation)
        ));
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function setConversions(
        string $experiment,
        string $variation,
        int $conversions
    ) {
        $this->aggregateStore->set(
            $this->keyGenerator->generateConversionKey($experiment, $variation),
            $conversions,
            $this->maxLifetime
        );
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    function addConversion(
        string $experiment,
        string $variation,
        int $conversions = 1
    ) {
        $currentConversions = $this->getConversions($experiment, $variation);
        $this->setConversions(
            $experiment,
            $variation,
            $currentConversions + $conversions
        );
    }
}
