<?php

namespace HeapsGoodServices\Variant\Event;

use HeapsGoodServices\Variant\EventRepository;
use HeapsGoodServices\Variant\EventRepositoryFactory;
use Psr\SimpleCache\CacheInterface;

class AggregateRepositoryFactory implements EventRepositoryFactory
{
    /**
     * @var CacheInterface
     */
    private $aggregateStore;

    /**
     * @var KeyGenerator
     */
    private $keyGenerator;

    /**
     * @var int|null
     */
    private $aggregateStoreLifetime;

    /**
     * AggregateEventRepositoryFactory constructor.
     * @param CacheInterface $aggregateStore
     * @param KeyGenerator $keyGenerator
     * @param int $aggregateStoreLifetime
     */
    public function __construct(CacheInterface $aggregateStore, KeyGenerator $keyGenerator, int $aggregateStoreLifetime = null)
    {
        $this->aggregateStore = $aggregateStore;
        $this->keyGenerator = $keyGenerator;
        $this->aggregateStoreLifetime = $aggregateStoreLifetime;
    }

    /**
     * @return EventRepository
     */
    function makeEventRepository(): EventRepository
    {
        return new AggregateRepository(
            $this->aggregateStore, $this->keyGenerator, $this->aggregateStoreLifetime
        );
    }
}
