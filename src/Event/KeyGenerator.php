<?php

namespace HeapsGoodServices\Variant\Event;

class KeyGenerator
{
    /**
     * @param string $experiment
     * @param string $variation
     * @return string
     */
    function generateInteractionKey(string $experiment, string $variation): string {
        return "variant.experiments.$experiment.variations.$variation.interactions";
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return string
     */
    function generateConversionKey(string $experiment, string $variation): string {
        return "variant.experiments.$experiment.variations.$variation.conversions";
    }

    /**
     * @param string $experiment
     * @return string
     */
    function generateVariationKey(string $experiment): string {
        return "variant.experiments.$experiment.variation";
    }
}
