<?php

namespace HeapsGoodServices\Variant\Event\Individual;

use HeapsGoodServices\Variant\Event\KeyGenerator;
use HeapsGoodServices\Variant\EventRepository;
use Psr\SimpleCache\CacheInterface;

class CacheRepository implements EventRepository
{
    /**
     * @var CacheInterface
     */
    private $individualCache;

    /**
     * @var KeyGenerator
     */
    private $keyGenerator;

    /**
     * @var int|null
     */
    private $cacheLifetime;

    /**
     * CacheRepository constructor.
     * @param CacheInterface $individualCache
     * @param KeyGenerator $keyGenerator
     * @param int|null $cacheLifetime
     */
    public function __construct(CacheInterface $individualCache, KeyGenerator $keyGenerator, int $cacheLifetime = null)
    {
        $this->individualCache = $individualCache;
        $this->keyGenerator = $keyGenerator;
        $this->cacheLifetime = $cacheLifetime;
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getInteractions(string $experiment, string $variation): int
    {
        return $this->individualCache->get($this->keyGenerator->generateInteractionKey($experiment, $variation), 0);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function addInteraction(string $experiment, string $variation, int $interactions = 1)
    {
        $incrementedInteractions = $this->getInteractions($experiment, $variation) + $interactions;
        $this->setInteraction($experiment, $variation, $incrementedInteractions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function setInteraction(string $experiment, string $variation, int $interactions)
    {
        $this->individualCache->set($this->keyGenerator->generateInteractionKey($experiment, $variation), $interactions, $this->cacheLifetime);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getConversions(string $experiment, string $variation): int
    {
        return $this->individualCache->get($this->keyGenerator->generateConversionKey($experiment, $variation), 0);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function addConversion(string $experiment, string $variation, int $conversions = 1)
    {
        $incrementedConversions = $this->getConversions($experiment, $variation) + $conversions;
        $this->setConversions($experiment, $variation, $incrementedConversions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function setConversions(string $experiment, string $variation, int $conversions)
    {
        $this->individualCache->set($this->keyGenerator->generateConversionKey($experiment, $variation), $conversions, $this->cacheLifetime);
    }
}
