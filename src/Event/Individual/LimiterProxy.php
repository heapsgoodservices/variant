<?php

namespace HeapsGoodServices\Variant\Event\Individual;

use HeapsGoodServices\Variant\EventRepository;

class LimiterProxy implements EventRepository
{
    /**
     * @var EventRepository
     */
    private $eventRepository;

    /**
     * LimiterProxy constructor.
     *
     * @param EventRepository $eventRepository
     */
    public function __construct(EventRepository $eventRepository) {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getInteractions(string $experiment, string $variation): int {
        return $this->eventRepository->getInteractions($experiment, $variation);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function addInteraction(
        string $experiment,
        string $variation,
        int $interactions = 1
    ) {
        if(!$currentInteractions = $this->eventRepository->getInteractions($experiment, $variation)) {
            $this->eventRepository->addInteraction($experiment, $variation, $interactions);
        }
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getConversions(string $experiment, string $variation): int {
        return $this->eventRepository->getConversions($experiment, $variation);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function addConversion(
        string $experiment,
        string $variation,
        int $conversions = 1
    ) {
        $currentConversions = $this->eventRepository->getConversions($experiment, $variation);

        if(!$currentConversions) {
            $this->eventRepository->addConversion($experiment, $variation, $conversions);
        }
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function setInteraction(
        string $experiment,
        string $variation,
        int $interactions
    ) {
        $this->eventRepository->setInteraction($experiment, $variation, $interactions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function setConversions(
        string $experiment,
        string $variation,
        int $conversions
    ) {
        $this->eventRepository->setConversions($experiment, $variation, $conversions);
    }
}
