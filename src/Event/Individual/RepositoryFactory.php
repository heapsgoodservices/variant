<?php

namespace HeapsGoodServices\Variant\Event\Individual;

use HeapsGoodServices\Variant\Event\AggregateRepositoryFactory;
use HeapsGoodServices\Variant\Event\KeyGenerator;
use HeapsGoodServices\Variant\EventRepositoryFactory;
use HeapsGoodServices\Variant\EventRepository;
use HeapsGoodServices\Variant\UserVerificationRepository;
use Psr\SimpleCache\CacheInterface;

class RepositoryFactory implements EventRepositoryFactory
{
    /**
     * @var AggregateRepositoryFactory
     */
    private $aggregateRepositoryFactory;

    /**
     * @var UserVerificationRepository
     */
    private $userVerificationRepository;

    /**
     * @var CacheInterface
     */
    private $individualCache;

    /**
     * @var KeyGenerator
     */
    private $keyGenerator;

    /**
     * @var int|null
     */
    private $individualCacheLifetime;

    /**
     * RepositoryFactory constructor.
     * @param CacheInterface $individualCache
     * @param AggregateRepositoryFactory $aggregateRepositoryFactory
     * @param UserVerificationRepository $userVerificationRepository
     * @param KeyGenerator $keyGenerator
     * @param int|null $individualCacheLifetime
     */
    public function __construct(
        CacheInterface $individualCache,
        AggregateRepositoryFactory $aggregateRepositoryFactory,
        UserVerificationRepository $userVerificationRepository,
        KeyGenerator $keyGenerator,
        int $individualCacheLifetime = null
    ) {
        $this->individualCache = $individualCache;
        $this->aggregateRepositoryFactory = $aggregateRepositoryFactory;
        $this->userVerificationRepository = $userVerificationRepository;
        $this->individualCacheLifetime = $individualCacheLifetime;
        $this->keyGenerator = $keyGenerator;
    }

    /**
     * @return EventRepository
     */
    function makeEventRepository(): EventRepository {
        $cacheRepository = new CacheRepository(
            $this->individualCache,
            $this->keyGenerator,
            $this->individualCacheLifetime
        );

        return new StatisticAggregatorDecorator(
            $cacheRepository,
            $this->aggregateRepositoryFactory->makeEventRepository(),
            $this->userVerificationRepository
        );
    }
}
