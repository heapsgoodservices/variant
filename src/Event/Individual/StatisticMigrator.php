<?php

namespace HeapsGoodServices\Variant\Event\Individual;

use HeapsGoodServices\Variant\Experiments;
use HeapsGoodServices\Variant\UserVariationRepository;
use HeapsGoodServices\Variant\EventRepository;

class StatisticMigrator
{
    /**
     * @var UserVariationRepository
     */
    private $userVariationRepository;

    /**
     * @var EventRepository
     */
    private $aggregateEventRepository;

    /**
     * @var EventRepository
     */
    private $individualEventRepository;

    /**
     * @var Experiments
     */
    private $experiments;

    /**
     * StatisticMigrator constructor.
     * @param UserVariationRepository $userVariationRepository
     * @param EventRepository $aggregateEventRepository
     * @param EventRepository $individualEventRepository
     * @param Experiments $experiments
     */
    public function __construct(
        UserVariationRepository $userVariationRepository,
        EventRepository $aggregateEventRepository,
        EventRepository $individualEventRepository,
        Experiments $experiments
    ) {
        $this->userVariationRepository = $userVariationRepository;
        $this->aggregateEventRepository = $aggregateEventRepository;
        $this->individualEventRepository = $individualEventRepository;
        $this->experiments = $experiments;
    }

    /**
     * Migrate the cached events to the aggregate repository.
     */
    function migrate() {
        foreach($this->experiments->getExperiments() as $experiment) {
            $cachedVariation = $this->userVariationRepository->getVariationForExperiment($experiment);
            if(is_null($cachedVariation)) {
                continue;
            }

            $cachedInteractions = $this->individualEventRepository->getInteractions($experiment->getName(), $cachedVariation);
            $this->aggregateEventRepository->addInteraction($experiment->getName(), $cachedVariation, $cachedInteractions);

            $cachedConversions = $this->individualEventRepository->getConversions($experiment->getName(), $cachedVariation);
            $this->aggregateEventRepository->addConversion($experiment->getName(), $cachedVariation, $cachedConversions);
        }
    }
}
