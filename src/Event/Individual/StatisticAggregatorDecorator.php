<?php

namespace HeapsGoodServices\Variant\Event\Individual;

use HeapsGoodServices\Variant\EventRepository;
use HeapsGoodServices\Variant\UserVerificationRepository;

/**
 * Class StatisticAggregatorDecorator
 *
 * This is a decorator for the instance repository. If the instance is verified
 * then the data is stored in the aggregate repository.
 *
 * @package HeapsGoodServices\Variant
 */
class StatisticAggregatorDecorator implements EventRepository
{
    /**
     * @var EventRepository
     */
    private $individualEventRepository;

    /**
     * @var EventRepository
     */
    private $aggregateEventRepository;

    /**
     * @var \HeapsGoodServices\Variant\UserVerificationRepository
     */
    private $verificationRepository;

    /**
     * InstanceStatisticAggregatorDecorator constructor.
     *
     * @param EventRepository $individualEventRepository
     * @param EventRepository $aggregateEventRepository
     * @param \HeapsGoodServices\Variant\UserVerificationRepository $verificationRepository
     */
    public function __construct(
        EventRepository $individualEventRepository,
        EventRepository $aggregateEventRepository,
        UserVerificationRepository $verificationRepository
    ) {
        $this->individualEventRepository = $individualEventRepository;
        $this->aggregateEventRepository = $aggregateEventRepository;
        $this->verificationRepository = $verificationRepository;
    }


    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getInteractions(string $experiment, string $variation): int {
        return $this->individualEventRepository->getInteractions($experiment, $variation);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function addInteraction(string $experiment, string $variation, int $interactions = 1) {
        $this->individualEventRepository->addInteraction($experiment, $variation, $interactions);

        if($this->verificationRepository->checkMultipleVerifications()) {
            $this->aggregateEventRepository->addInteraction($experiment, $variation, $interactions);
        }
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getConversions(string $experiment, string $variation): int {
        return $this->individualEventRepository->getConversions($experiment, $variation);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function addConversion(string $experiment, string $variation, int $conversions = 1) {
        $this->individualEventRepository->addConversion($experiment, $variation, $conversions);

        if($this->verificationRepository->checkMultipleVerifications()) {
            $this->aggregateEventRepository->addConversion($experiment, $variation, $conversions);
        }
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function setInteraction(
        string $experiment,
        string $variation,
        int $interactions
    ) {
        $this->individualEventRepository->setInteraction($experiment, $variation, $interactions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function setConversions(
        string $experiment,
        string $variation,
        int $conversions
    ) {
        $this->individualEventRepository->setConversions($experiment, $variation, $conversions);
    }
}
