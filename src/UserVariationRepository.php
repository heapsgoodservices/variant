<?php

namespace HeapsGoodServices\Variant;

interface UserVariationRepository
{
    /**
     * @param Experiment $experiment
     * @return string|null
     */
    function getVariationForExperiment(Experiment $experiment);

    /**
     * @param Experiment $experiment
     * @param Variation $variation
     */
    function setVariationForExperiment(Experiment $experiment, Variation $variation);
}
