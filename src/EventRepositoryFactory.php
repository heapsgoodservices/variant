<?php

namespace HeapsGoodServices\Variant;

interface EventRepositoryFactory
{
    /**
     * @return EventRepository
     */
    function makeEventRepository(): EventRepository;
}
