<?php

namespace HeapsGoodServices\Variant;

use HeapsGoodServices\Variant\Event\Individual\LimiterProxy;
use InvalidArgumentException;

class VariationFactory
{
    /**
     * @var EventRepositoryFactory
     */
    private $aggregateSimpleRepositoryFactory;

    /**
     * @var EventRepositoryFactory
     */
    private $individualRepositoryFactory;

    /**
     * VariationFactory constructor.
     * @param EventRepositoryFactory $aggregateSimpleRepositoryFactory
     * @param EventRepositoryFactory $individualRepositoryFactory
     */
    public function __construct(EventRepositoryFactory $aggregateSimpleRepositoryFactory, EventRepositoryFactory $individualRepositoryFactory) {
        $this->aggregateSimpleRepositoryFactory = $aggregateSimpleRepositoryFactory;
        $this->individualRepositoryFactory = $individualRepositoryFactory;
    }

    /**
     * @param string $experimentationName
     * @param string $name
     * @param int $weight
     * @param bool $isSystemExperiment
     * @return Variation
     * @throws InvalidArgumentException
     */
    function makeVariation(
        string $experimentationName,
        string $name,
        int $weight,
        bool $isSystemExperiment = false
    ): Variation {
        if($weight <= 0) {
            throw new InvalidArgumentException("Weight must be greater than zero.");
        }

        if($isSystemExperiment) {
            $eventRepository = $this->aggregateSimpleRepositoryFactory->makeEventRepository();
        } else {
            $eventRepository = new LimiterProxy(
                $this->individualRepositoryFactory->makeEventRepository()
            );
        }

        return new Variation(
            $experimentationName,
            $name,
            $weight,
            $eventRepository
        );
    }
}
