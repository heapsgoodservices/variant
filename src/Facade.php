<?php

namespace HeapsGoodServices\Variant;

use HeapsGoodServices\Variant\Event\Individual\StatisticMigrator;
use InvalidArgumentException;

class Facade
{
    /**
     * @var EventRepositoryFactory
     */
    private $individualEventRepositoryFactory;

    /**
     * @var EventRepositoryFactory
     */
    private $aggregateEventRepositoryFactory;

    /**
     * @var UserVerificationRepository
     */
    private $verificationRepository;

    /**
     * @var Experiments
     */
    private $experimentStore;

    /**
     * @var WeightedRandomiser
     */
    private $weightedRandomiser;

    /**
     * @var SimpleExperimentFactory
     */
    private $experimentFactory;

    /**
     * @var StatisticMigrator
     */
    private $statisticMigrator;

    /**
     * Facade constructor.
     *
     * @param EventRepositoryFactory $individualEventRepositoryFactory
     * @param EventRepositoryFactory $aggregateEventRepositoryFactory
     * @param UserVerificationRepository $verificationRepository
     * @param Experiments $experimentStore
     * @param WeightedRandomiser $weightedRandomiser
     * @param SimpleExperimentFactory $experimentFactory
     * @param StatisticMigrator $statisticMigrator
     */
    public function __construct(
        EventRepositoryFactory $individualEventRepositoryFactory,
        EventRepositoryFactory $aggregateEventRepositoryFactory,
        UserVerificationRepository $verificationRepository,
        Experiments $experimentStore,
        WeightedRandomiser $weightedRandomiser,
        SimpleExperimentFactory $experimentFactory,
        StatisticMigrator $statisticMigrator
    ) {
        $this->individualEventRepositoryFactory = $individualEventRepositoryFactory;
        $this->aggregateEventRepositoryFactory = $aggregateEventRepositoryFactory;
        $this->verificationRepository = $verificationRepository;
        $this->experimentStore = $experimentStore;
        $this->weightedRandomiser = $weightedRandomiser;
        $this->experimentFactory = $experimentFactory;
        $this->statisticMigrator = $statisticMigrator;
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getAggregateInteractions(string $experiment, string $variation): int {
        return $this->aggregateEventRepositoryFactory->makeEventRepository()->getInteractions($experiment, $variation);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function setAggregateInteractions(string $experiment, string $variation, int $interactions) {
        $this->aggregateEventRepositoryFactory->makeEventRepository()->setInteraction($experiment, $variation, $interactions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $interactions
     */
    function addAggregateInteraction(string $experiment, string $variation, int $interactions = 1) {
        $this->aggregateEventRepositoryFactory->makeEventRepository()
            ->addInteraction($experiment, $variation, $interactions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @return int
     */
    function getAggregateConversions(string $experiment, string $variation): int {
        return $this->aggregateEventRepositoryFactory->makeEventRepository()->getConversions($experiment, $variation);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function setAggregateConversions(string $experiment, string $variation, int $conversions) {
        $this->aggregateEventRepositoryFactory->makeEventRepository()->setConversions($experiment, $variation, $conversions);
    }

    /**
     * @param string $experiment
     * @param string $variation
     * @param int $conversions
     */
    function addAggregateConversion(string $experiment, string $variation, int $conversions = 1) {
        $this->aggregateEventRepositoryFactory->makeEventRepository()
            ->addConversion($experiment, $variation, $conversions);
    }

    /**
     * Image verification complete.
     */
    function setImageVerification() {
        // Check if verification is complete.
        if($this->verificationRepository->checkMultipleVerifications()) {
            return;
        }
        $this->verificationRepository->imageVerificationConfirmed();
        if(!$this->verificationRepository->checkScriptVerification()) {
            return;
        }
        $this->verificationRepository->multipleVerificationsConfirmed();
        $this->statisticMigrator->migrate();
    }

    /**
     * Script verification complete.
     */
    function setScriptVerification() {
        // Check if verification is complete.
        if($this->verificationRepository->checkMultipleVerifications()) {
            return;
        }
        $this->verificationRepository->scriptVerificationConfirmed();
        if(!$this->verificationRepository->checkImageVerification()) {
            return;
        }
        $this->verificationRepository->multipleVerificationsConfirmed();
        $this->statisticMigrator->migrate();
    }

    /**
     * @param string $experimentName
     * @return Experiment
     * @throws InvalidArgumentException
     */
    function getExperiment(string $experimentName): Experiment {
        if(!$this->experimentStore->hasExperiment($experimentName)) {
            throw new InvalidArgumentException("Experiment '{$experimentName}' does not exit.");
        }

        return $this->experimentStore->getExperiment($experimentName);
    }

    /**
     * @return array|Experiment[]
     */
    function getExperiments(): array {
        return $this->experimentStore->getExperiments();
    }

    /**
     * @param string $name
     * @param bool $isSystemExperiment
     * @return Experiment
     */
    function registerExperiment(
        string $name,
        bool $isSystemExperiment = false
    ): Experiment {
        $experiment = $this->experimentFactory->makeExperiment(
            $name,
            $isSystemExperiment
        );

        $this->experimentStore->addExperiment($experiment);

        return $experiment;
    }
}
